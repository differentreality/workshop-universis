import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileHomeComponent } from './components/profile-home/profile-home.component';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ProfileRoutingModule} from './profile-routing.routing';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import {ProfileSharedModule} from './profile-shared.module';
import {SharedModule} from '@universis/common';
import { StudentsSharedModule } from '../students-shared/students-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    MostModule,
    ProfileSharedModule,
    ProfileRoutingModule,
    StudentsSharedModule
  ],
  declarations: [
      ProfileHomeComponent,
      ProfilePreviewComponent
  ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [

  ]
})
export class ProfileModule {

    constructor() {
        //
    }
}
